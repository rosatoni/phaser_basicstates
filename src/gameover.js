var gameOver = function(game){}

gameOver.prototype = {
	init: function(score){
		alert("Game over! Score:" + score);
	},
	create: function(){
  	var gameOverTitle = this.game.add.sprite(160, 160, "gameover");
		gameOverTitle.anchor.setTo(0.5,0.5);
		var backToMenuButton = this.game.add.button(160, 320, "back", this.backToMenu, this);
		backToMenuButton.anchor.setTo(0.5,0.5);
	},
	backToMenu: function(){
		this.game.state.start("Menu");
	}
}
