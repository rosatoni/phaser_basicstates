var Game = function(phaserGameInstance){
	// This init function gets executed immediately when --> game.state.add("Playing", Game); <-- is called)
	score = 99;
}

Game.prototype = {
	preload: function(){
	},
	init: function(){
	},
  create: function(phaserGameInstance){
		phaserGameInstance.add.text(5, 5, 'Just wait for 4 seconds', { font: '18px Arial', fill: '#0095DD' });
		setTimeout( ()=>{
			phaserGameInstance.state.start("GameOver", true, false, score /* should be 99 as defined in the object constructor */);
		} , 4000);
	},
	update: function(phaserGameInstance){
		
	},
	render: function(phaserGameInstance){

	}
}
