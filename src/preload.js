var preload = function(game){}

preload.prototype = {
	preload: function(){
      var loadingBar = this.add.sprite(160,240,"loading");
      loadingBar.anchor.setTo(0.5,0.5);
      this.load.setPreloadSprite(loadingBar);
			this.game.load.image("gametitle","assets/gametitle.png");
			this.game.load.image("start","assets/start.png");
			this.game.load.image("back","assets/back.png");
			this.game.load.image("gameover","assets/gameover.png");
	},
  create: function(){
				this.game.state.start("Menu");
	}
}
