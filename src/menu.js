var menu = function(game){}

menu.prototype = {
  	create: function(){
		var gameTitle = this.game.add.sprite(160,160,"gametitle");
		gameTitle.anchor.setTo(0.5,0.5);
		var playButton = this.game.add.button(160,320,"start",this.play,this);
		playButton.anchor.setTo(0.5,0.5);
	},
	play: function(){
		this.game.state.start("Playing");
	}
}
